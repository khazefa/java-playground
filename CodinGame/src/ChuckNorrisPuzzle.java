import java.util.*;
import java.io.*;
import java.math.*;

public class ChuckNorrisPuzzle {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter your message here: ");
        String msg = in.nextLine();

        // String encodedMsg = null;
        StringBuilder encodedMsg = new StringBuilder();
        char[] msgChar = msg.toCharArray();

        // for (int i = 0; i < msgChar.length; i++) {
        //     binaryStr.append(Integer.toBinaryString(msgChar[i]));
        // }

        for (char aChar : msgChar) {
            // encodedMsg.append("0 " + Integer.toBinaryString(aChar) + " ");
            String block = Integer.toBinaryString(aChar) + "";
            encodeMessage(block);
            encodedMsg.append( encodeMessage(block) );
            // System.out.println("first block: " + block.substring(0, 4));
            // System.out.println("second block: " + block.substring(3, 7));
        }

        // Write an answer using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(encodedMsg);
    }

    private static String encodeMessage(String binaryStr) {
        String encodedMsg = "";
        encodedMsg += binaryStr.replaceAll("0", "X");
        return encodedMsg;
    }
}
